FROM python:3.8-alpine
ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
RUN pip install --upgrade pipenv
WORKDIR /tic_tac_toe
COPY . /tic_tac_toe/
RUN apk update
RUN apk add bash
RUN apk add --no-cache --update python3
RUN apk add --no-cache postgresql-libs
RUN apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev
RUN apk add python3-dev build-base linux-headers pcre-dev libffi-dev openssl-dev
RUN apk add rust cargo
COPY Pipfile Pipfile.lock /tic_tac_toe/
RUN pipenv install --system --dev
COPY ./docker-entrypoint.sh /tic_tac_toe/docker-entrypoint.sh
RUN chmod a+x /tic_tac_toe/docker-entrypoint.sh
ENTRYPOINT ["./docker-entrypoint.sh"]
