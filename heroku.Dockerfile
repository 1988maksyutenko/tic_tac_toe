FROM python:3.8-alpine
ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
RUN pip install --upgrade pipenv
RUN mkdir -p /home/user
RUN addgroup -S user && adduser -S user -G user
ENV HOME=/home/user
ENV APP_HOME=/home/user/tic_tac_toe
RUN mkdir $APP_HOME
WORKDIR $APP_HOME
COPY . $APP_HOME/
RUN apk update
RUN apk add bash
RUN apk add --no-cache --update python3
RUN apk add --no-cache postgresql-libs
RUN apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev
RUN apk add python3-dev build-base linux-headers pcre-dev libffi-dev openssl-dev
RUN apk add rust cargo
COPY Pipfile Pipfile.lock $APP_HOME/
RUN pipenv install --system --deploy
#COPY ./docker-entrypoint.prod.sh $APP_HOME/
RUN python manage.py collectstatic --noinput
#RUN chmod a+x $APP_HOME/docker-entrypoint.prod.sh
RUN chown -R user:user $APP_HOME
USER user
CMD python manage.py migrate && daphne tic_tac_toe.asgi:application -b 0.0.0.0 -p $PORT
