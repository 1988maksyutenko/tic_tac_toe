from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'tic_tac_toe.core'
