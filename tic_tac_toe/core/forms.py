from django import forms
from django.contrib.auth.forms import AuthenticationForm

from .models import BaseUser


class CreateUserForm(forms.ModelForm):
    username = forms.CharField(max_length=20, label='user name')
    password = forms.CharField(widget=forms.PasswordInput(), label='password')

    class Meta:
        model = BaseUser
        fields = ['username', 'password']

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data['password'])
        if commit:
            user.save()
        return user


class LoginUserForm(AuthenticationForm):
    username = forms.CharField(max_length=20, label='user name')
    password = forms.CharField(widget=forms.PasswordInput(), label='password')
