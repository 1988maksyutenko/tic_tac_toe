from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.contrib.auth import get_user_model
from django.db import models


class CustomUserManages(BaseUserManager):
    def create_user(self, user_name, password=None):
        return get_user_model().create(
            username=user_name,
            password=password

        )

    def create_superuser(self, user_name, password):
        return get_user_model().create(
            username=user_name,
            password=password,
            is_superuser=True
        )


class BaseUser(AbstractUser):
    updated = models.DateTimeField(null=True, blank=True)

    manager = CustomUserManages

    class Meta:
        db_table = 'User'
