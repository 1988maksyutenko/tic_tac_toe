from django.urls import path
from django.contrib.auth.views import LoginView

from .views import home, logout_view, signin, rating, user_quit
from .forms import LoginUserForm

urlpatterns = [
    path('', home, name='home'),
    path('login/', LoginView.as_view(template_name='login.html', authentication_form=LoginUserForm), name='login'),
    path('signup/', signin, name='signup'),
    path('logout/', logout_view, name='logout'),
    path('rating/', rating, name='rating'),
    path('user_quit/', user_quit, name='user_quit')
]