from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from django.contrib.auth import login, logout, get_user_model

from tic_tac_toe.game.models import GameField, Statistic
from .forms import CreateUserForm

User = get_user_model()


def home(request):
    if request.method == 'GET':
        form = CreateUserForm()
        fields = GameField.objects.all()
        return render(request, 'index.html', {'form': form, 'fields': fields})


def rating(request):
    if request.method == 'GET':
        rating = Statistic.objects.all().order_by('-wins_total')
        paginator = Paginator(rating, 5)
        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)
        return render(request, 'rating.html', {'page_obj': page_obj})


def signin(request):
    if request.method == 'GET':
        form = CreateUserForm()
        return render(request, 'signup.html', {'form': form})
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            return redirect('home')
        return render(request, 'index.html', {})


def logout_view(request):
    try:
        GameField.objects.get(game_name=request.user).delete()
    except GameField.DoesNotExist:
        pass
    logout(request)
    return redirect('home')


def user_quit(request):
    return render(request, 'user_quit.html', {})
