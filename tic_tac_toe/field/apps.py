from django.apps import AppConfig


class FieldConfig(AppConfig):
    name = 'tic_tac_toe.field'
