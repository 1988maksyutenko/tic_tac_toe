from channels.generic.websocket import AsyncJsonWebsocketConsumer

from .consumer_mixin import GameFieldMixin, GameFieldDBMixin, GameFieldEventMixin


class GameFieldConsumer(
    AsyncJsonWebsocketConsumer,
    GameFieldMixin,
    GameFieldDBMixin,
    GameFieldEventMixin
):

    async def connect(self):
        await self.set_group_name()
        await self.channel_layer.group_add(self.group_name, self.channel_name)
        await self.accept()
        await self.channel_layer.group_send(self.group_name, await self.update_user_data())

    async def disconnect(self, code):
        pass

    async def receive_json(self, content, **kwargs):
        group_name = content['data']['group']
        content['data']['user_id'] = self.scope['user'].id
        data = await self.response(content['data'])
        await self.channel_layer.group_send(group_name, data)
