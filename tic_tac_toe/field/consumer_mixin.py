from channels.db import database_sync_to_async

from tic_tac_toe.game.models import GameField, Statistic


class GameFieldMixin:
    def __init__(self):
        self.group_name = None

    async def set_group_name(self):
        if self.scope['user'].username in self.scope['path']:
            self.group_name = self.scope['user'].username
        else:
            self.group_name = self.scope['path'].split('/')[-2]

    async def is_win_combination(self, field):
        if (field[0] == ['X', 'X', 'X']) or (field[0] == ['O', 'O', 'O']):
            return True
        elif (field[1] == ['X', 'X', 'X']) or (field[1] == ['O', 'O', 'O']):
            return True
        elif (field[2] == ['X', 'X', 'X']) or (field[2] == ['O', 'O', 'O']):
            return True
        elif (field[0][0] == 'X' and field[1][0] == 'X' and field[2][0] == 'X') or\
            (field[0][0] == 'O' and field[1][0] == 'O' and field[2][0] == 'O'):
            return True
        elif (field[0][1] == 'X' and field[1][1] == 'X' and field[2][1] == 'X') or \
                (field[0][1] == 'O' and field[1][1] == 'O' and field[2][1] == 'O'):
            return True
        elif (field[0][2] == 'X' and field[1][2] == 'X' and field[2][2] == 'X') or \
                (field[0][2] == 'O' and field[1][2] == 'O' and field[2][2] == 'O'):
            return True
        elif (field[0][0] == 'X' and field[1][1] == 'X' and field[2][2] == 'X') or \
                (field[0][0] == 'O' and field[1][1] == 'O' and field[2][2] == 'O'):
            return True
        elif (field[0][2] == 'X' and field[1][1] == 'X' and field[2][0] == 'X') or \
                (field[0][2] == 'O' and field[1][1] == 'O' and field[2][0] == 'O'):
            return True
        return False


class GameFieldDBMixin:
    @database_sync_to_async
    def get_user_char(self):
        game = GameField.objects.get(game_name=self.group_name)
        if game.user_x == self.scope['user']:
            return 'X'
        else:
            return 'O'

    def update_statistic(self, game_user):
        user_win = Statistic.objects.get_or_create(user=game_user)[0]
        user_win.wins_total = user_win.wins_total + 1
        user_win.save()

    @database_sync_to_async
    def win_increment(self, data):
        game = GameField.objects.get(game_name=self.group_name)
        if data['user_id'] == game.user_x_id:
            game.user_x_win = game.user_x_win + 1
            self.update_statistic(game.user_x)
        else:
            game.user_o_win = game.user_o_win + 1
            self.update_statistic(game.user_o)
        game.save()

    @database_sync_to_async
    def reset_game_field(self):
        try:
            game = GameField.objects.get(game_name=self.group_name)
            game.chars_on_field = [['.', '.', '.'], ['.', '.', '.'], ['.', '.', '.']]
            game.save()
            return game.chars_on_field
        except GameField.DoesNotExist:
            return None

    @database_sync_to_async
    def get_game_field_chars(self):
        try:
            return GameField.objects.get(game_name=self.group_name).chars_on_field
        except GameField.DoesNotExist:
            return None


    @database_sync_to_async
    def is_has_opponent(self):
        game = GameField.objects.get(game_name=self.group_name)
        if game.user_x and game.user_o:
            return True
        return False

    @database_sync_to_async
    def is_user_move(self, data):
        game = GameField.objects.get(game_name=self.group_name)

        if data['user_id'] != game.user_move:
            return False

        if game.user_move == game.user_x_id:
            game.user_move = game.user_o_id
        else:
            game.user_move = game.user_x_id
        game.save()

        return True

    @database_sync_to_async
    def set_char_on_field(self, data):
        game = GameField.objects.get(game_name=self.group_name)
        plate = data['id']
        char = 'X' if data['user_id'] == game.user_x_id else 'O'
        if plate == '1':
            if game.chars_on_field[0][0] == '.':
                game.chars_on_field[0][0] = char
        elif plate == '2':
            if game.chars_on_field[0][1] == '.':
                game.chars_on_field[0][1] = char
        elif plate == '3':
            if game.chars_on_field[0][2] == '.':
                game.chars_on_field[0][2] = char
        elif plate == '4':
            if game.chars_on_field[1][0] == '.':
                game.chars_on_field[1][0] = char
        elif plate == '5':
            if game.chars_on_field[1][1] == '.':
                game.chars_on_field[1][1] = char
        elif plate == '6':
            if game.chars_on_field[1][2] == '.':
                game.chars_on_field[1][2] = char
        elif plate == '7':
            if game.chars_on_field[2][0] == '.':
                game.chars_on_field[2][0] = char
        elif plate == '8':
            if game.chars_on_field[2][1] == '.':
                game.chars_on_field[2][1] = char
        elif plate == '9':
            if game.chars_on_field[2][2] == '.':
                game.chars_on_field[2][2] = char
        game.save()

    @database_sync_to_async
    def get_game_info(self):
        game = GameField.objects.get(game_name=self.group_name)
        return {
            'game_played': game.game_count,
            'x_win': game.user_x_win,
            'o_win': game.user_o_win,
            'user_move': game.user_x.username if game.user_move == game.user_x_id else game.user_o.username
        }


class GameFieldEventMixin:

    async def response(self, data):
        if not await self.is_has_opponent():
            return {'type': 'no_opponent', 'data': 'There are no opponent!'}
        event_type = data['event']
        return {'type': event_type, 'data': await self.get_response_data(event_type, data)}

    async def get_response_data(self, event_type, data):
        if event_type == 'user_move':
            data = {'id': data['field'], 'user_id': data['user_id']}
            await self.before_user_move(data)
            return data
        elif event_type == 'game_info':
            return {}
        elif event_type == 'quit':
            return {}

    async def quit(self, event):
        await self.send_json({
            'data': {
                'event': 'quit'
            }
        })

    async def game_info(self, event):
        await self.send_json({
            'data': {
                'event': 'game_info',
                'game_info': await self.get_game_info()
            }
        })

    async def update_user_data(self):
        return {
            'type': 'set_user_char',
            'data': {
                'event': 'set_user_char',
                'char': await self.get_user_char(),
                'user': self.scope['user'].username,
                'game_field': await self.get_game_field_chars(),
                'game_info': await self.get_game_info()
            }
        }

    async def no_opponent(self, event):
        data = event['data']
        await self.send_json({'data': data})

    async def before_user_move(self, data):
        if await self.is_user_move(data):
            await self.set_char_on_field(data)
        response_data = await self.get_game_field_chars()
        if await self.is_win_combination(response_data):
            await self.win_increment(data)
            await self.reset_game_field()

    async def user_move(self, event):
        response_data = await self.get_game_field_chars()
        await self.send_json({
            'data': {
                'event': 'user_move',
                'game_field': response_data
            }
        })

    async def set_user_char(self, event):
        data = event['data']
        await self.send_json({'data': data})
