from django.db import models
from django.contrib.postgres.fields import ArrayField


class Field(models.Model):
    class Meta:
        db_table = 'field"."field'

    blocks_num = models.IntegerField()
    block_size = models.IntegerField()
    blocks_color = models.CharField(max_length=20, null=False, blank=True)
    win_combinations = ArrayField(ArrayField(models.IntegerField(), size=3), size=8)
