from django.urls import re_path

from .consumer import GameFieldConsumer

websocket_urlpatterns = (
    re_path(r'ws/game/(?P<field>\w+)/$', GameFieldConsumer.as_asgi()),
)
