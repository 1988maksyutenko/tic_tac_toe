const gameName = window.location.pathname
const groupName = window.location.pathname.split('/').slice(2, 3)[0]
const socket = new WebSocket('ws://' + window.location.host + '/ws' + gameName)
const gamePlates = document.querySelector('#plates')
const userMoveIndicator = document.querySelector('#userMove')

function quit(e) {
    if (socket.OPEN) {
        socket.send(
            JSON.stringify({'data': {'event': 'quit', 'group': groupName}})
        )
    }
}

function userMove(e) {
    if (socket.OPEN) {
        socket.send(
            JSON.stringify(
                {'data':
                    {
                        'event': 'user_move',
                        'field': e.id,
                        'group': groupName
                    }
                }
            )
        )
        socket.send(
            JSON.stringify({'data': {'event': 'game_info', 'group': groupName}})
        )
    }
}

function updateUserInfo(data) {
    if (data.char === 'X') {
        document.querySelector('#userX').innerHTML = 'X: ' + data.user
    } else {
        document.querySelector('#userO').innerHTML = 'O: ' + data.user
    }
    document.querySelector('#userXwin').innerHTML = ' win: ' + data.game_info.x_win
    document.querySelector('#userOwin').innerHTML = ' win: ' + data.game_info.o_win
    if (data.user !== data.game_info.user_move) {
        userMoveIndicator.innerHTML = data.game_info.user_move + ' move'
    }
}

function updateGamePlates(data) {
    const plates = gamePlates.getElementsByTagName('button')
    const chars = data.game_field.flat()
    for (let i = 0; i < plates.length; i++) {
        plates[i].innerHTML = chars[i]
    }
}

function updateGameInfo(data) {
    userMoveIndicator.innerHTML = data.game_info.user_move + ' move'
    document.querySelector('#userXwin').innerHTML = ' win: ' + data.game_info.x_win
    document.querySelector('#userOwin').innerHTML = ' win: ' + data.game_info.o_win
}

function quitRedirect() {
    window.location.href = window.location.origin + '/user_quit/'
}

function eventHandling(data) {
    switch (data.event) {
        case 'set_user_char':
            updateUserInfo(data)
            updateGamePlates(data)
        break
        case 'user_move':
            updateGamePlates(data)
        break
        case 'game_info':
            updateGameInfo(data)
        break
        case 'quit':
            quitRedirect()
        break
    }
}

socket.onclose = () => {
    socket.send(JSON.stringify({'data': {'event': 'close'}}))
}

socket.onmessage = (e) => {
    const data = JSON.parse(e.data).data
    eventHandling(data)
}
