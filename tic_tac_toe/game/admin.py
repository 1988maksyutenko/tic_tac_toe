from django.contrib import admin
from .models import GameField, Statistic

# Register your models here.
admin.site.register(GameField)
admin.site.register(Statistic)
