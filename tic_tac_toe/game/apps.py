from django.apps import AppConfig


class GameConfig(AppConfig):
    name = 'tic_tac_toe.game'
