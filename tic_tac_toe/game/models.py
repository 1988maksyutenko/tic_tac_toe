from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import ArrayField
from django.db import models

User = get_user_model()


class GameField(models.Model):
    class Meta:
        db_table = 'game"."game_field'

    game_name = models.CharField(max_length=256, null=False, blank=False)
    user_x = models.ForeignKey(User, on_delete=models.PROTECT, related_name='user_char_x', null=True)
    user_o = models.ForeignKey(User, on_delete=models.PROTECT, related_name='user_char_o', null=True)
    user_x_win = models.IntegerField(default=0)
    user_o_win = models.IntegerField(default=0)
    game_count = models.IntegerField(default=0)
    chars_on_field = ArrayField(
        ArrayField(
            models.CharField(max_length=1, null=True, blank=True),
            size=3,
            null=True
        ),
        size=3,
        null=True
    )
    user_move = models.IntegerField(default=None, null=True, blank=True)


class Statistic(models.Model):
    class Meta:
        db_table = 'game"."statistic'

    user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='statistic')
    wins_total = models.IntegerField(default=0)
