from .models import GameField


class GameFieldService:

    def __init__(self, field):
        try:
            self.game_field = GameField.objects.get(game_name=field)
        except GameField.DoesNotExist:
            raise GameField.DoesNotExist()

    def _set_game_owner(self, request, user):
        user_char = request.POST['char']
        if user_char == 'X':
            self.game_field.user_x_id = user.id
        elif user_char == 'O':
            self.game_field.user_o_id = user.id
        self.game_field.save()

    def _set_opponent(self, request, user):
        if request.user.username != self.game_field.game_name:
            if self.game_field.user_o_id:
                self.game_field.user_x_id = user.id
            else:
                self.game_field.user_o_id = user.id
            self.game_field.save()

    def get_data(self):
        return {
            'game_field': self.game_field,
            'gameCount': self.game_field.game_count,
            'userX': self.game_field.user_x,
            'userO': self.game_field.user_o,
            'userXwin': self.game_field.user_x_win,
            'userOwin': self.game_field.user_o_win,
            'charsOnField': self.game_field.chars_on_field
        }

    def set_user_character(self, request):
        if self.game_field.user_x and self.game_field.user_o:
            return
        user = request.user
        if request.method == 'GET':
            self._set_opponent(request, user)
        if request.method == 'POST':
            self._set_game_owner(request, user)
