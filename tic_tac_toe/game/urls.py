from django.urls import path

from .views import select_char, select_game_field, quit_game

urlpatterns = [
    path('select-char/', select_char, name='select_character'),
    path('<str:field>/', select_game_field, name='game_field'),
    path('quit', quit_game, name='quit_game')
]
