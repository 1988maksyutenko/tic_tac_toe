from django.db.models import Q
from django.shortcuts import render, redirect

from .models import GameField
from .services import GameFieldService


def select_char(request):
    if request.method == 'GET':
        chars = "{{., ., .}, {., ., .}, {., ., .}}"
        GameField.objects.get_or_create(
            game_name=request.user,
            chars_on_field=chars,
            user_move=request.user.id
        )
        return render(request, 'char_select.html', {'field': request.user})


def select_game_field(request, field):
    try:
        game_field = GameFieldService(field)
    except GameField.DoesNotExist as e:
        return render(request, 'game_not_exist.html', {})
    if request.method == 'GET':
        game_field.set_user_character(request)
        return render(request, 'game.html', game_field.get_data())
    if request.method == 'POST':
        game_field.set_user_character(request)
        return render(request, 'game.html', game_field.get_data())


def quit_game(request):
    game = GameField.objects.filter(Q(user_o_id=request.user.id) | Q(user_x_id=request.user.id))
    game.delete()
    return redirect('home')
